Source: opentaxii
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-setuptools,
 python3-all,
 python3-blinker,
 python3-flask,
 python3-jwt,
 python3-libtaxii,
 python3-marshmallow,
 python3-sqlalchemy,
 python3-stix2,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 python3-structlog,
 python3-yaml
Testsuite: autopkgtest-pkg-python
Standards-Version: 4.6.2
Homepage: https://github.com/eclecticiq/OpenTAXII
Vcs-Browser: https://gitlab.com/kalilinux/packages/opentaxii
Vcs-Git: https://gitlab.com/kalilinux/packages/opentaxii.git

Package: opentaxii
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
Suggests: opentaxii-doc
Description: TAXII server implementation from EclecticIQ
 This package contains a robust Python implementation of TAXII Services that
 delivers rich feature set and friendly pythonic API built on top of well
 designed application.
 .
 OpenTAXII is guaranteed to be compatible with Cabby, TAXII client library.

Package: opentaxii-doc
Section: doc
Architecture: all
Depends:
 ${sphinxdoc:Depends},
 ${misc:Depends},
Description: TAXII server implementation from EclecticIQ (common documentation)
 This package contains a robust Python implementation of TAXII Services that
 delivers rich feature set and friendly pythonic API built on top of well
 designed application.
 .
 OpenTAXII is guaranteed to be compatible with Cabby, TAXII client library.
 .
 This is the common documentation package.
